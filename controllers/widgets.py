"""Widgets objects personalized module"""

from npyscreen import ButtonPress, GridColTitles


class ActionButton(ButtonPress):

    def whenPressed(self):
        # self.parent._text.value = "HOP !"
        self.parent.parentApp.switchForm("TASK")
        self.parent.display()


class TaskTable(GridColTitles):

    def custom_print_cell(self,
                          actual_cell,
                          cell_display_value):

        if len(cell_display_value) <= 5:
            actual_cell.column_width = 7
        if cell_display_value == "i3wm":
            actual_cell.color = 'DANGER'
        else:
            actual_cell.color = 'GOOD'

