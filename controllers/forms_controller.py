"""Tasks controller module"""

from models import TaskWarrior
from datetime import datetime
from npyscreen import notify_yes_no, notify_confirm


class TaskController:
    """Task instance controller"""

    def __init__(self, form, observer):
        """Init control tasks"""

        self._form = form
        self._observer = observer

    def beforeEditing(self):

        warrior = TaskWarrior(str(self._observer.get("task")[0]))
        task = warrior.get("report")
        _id = task['id']
        self._form.name = str('Report task n° {0:>3}').format(_id)
        self._form._uuid.value = task['uuid']
        self._form._project.value = task['project']
        self._form._date_entry.value = datetime.strptime(task['entry'], '%Y%m%dT%H%M%SZ')
        self._form._description.values = [task['description']]
        self._form._status.value = task['status']
        self._form._urgency.value = task['urgency']
        if "tags" in task:
            self._form._tags.values = task['tags']
        else:
            self._form._tags.hidden = True
        if "start" in task:
            self._form._date_start.value = datetime.strptime(task["start"], '%Y%m%dT%H%M%SZ')
            self._form._date_end.hidden = False
        else:
            self._form._date_start.hidden = True
            self._form._date_end.hidden = True
        if "end" in task:
            self._form._date_start.value = datetime.strptime(task["end"], '%Y%m%dT%H%M%SZ')

    def afterEditing(self):

        pass
        # self._form._observer.set_data("text", self._text.value)
        # self._form.parentApp.setNextForm(None)

    def on_ok(self):

        self._form.parentApp.switchForm("MAIN")

    def on_cancel(self):

        cancel = notify_yes_no("Do you want to cancel ?")
        if cancel:
            self._form.parentApp.switchForm("MAIN")


class TasksListController:
    """Tasks list instance controller"""

    def __init__(self, form, observer):

        self._form = form
        self._observer = observer

    def beforeEditing(self):

        tasks = TaskWarrior("status:pending")
        self._form._grid.col_titles = ["ID", "Project", "Description", "Urgency"]
        self._form._grid.select_whole_line = True
        self._form._grid.show_scroll = True
        self._form._grid.values = tasks.get("array")

    def afterEditing(self):

        self._observer.set("text", self._form._text.value)
        try:
            self._observer.set("task", self._form._grid.selected_row())
        except:
            pass

    def while_editing(self, *args, **kwargs):

        pass

    def while_waiting(self):

        self._form.parentApp.switchForm(None)

    def on_ok(self):

        exit = notify_yes_no("Do you want to exit ?")
        if exit:
            self._form.parentApp.switchForm(None)

    def menu_show_edit(self):

        try:
            if self._form._grid.selected_row():
                self._form.parentApp.switchForm("TASK")
        except:
            notify_confirm("You have to, first, select a task in the list",
                           "WARNING")

    def menu_report_logs(self):

        pass