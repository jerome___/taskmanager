"""Manager module controller"""


from . import TaskController, TasksListController
from views import ManagerViews
from models import StructureTasks


class ManagerController:
    """Application Main Controller"""

    def __init__(self):
        """Initialization for Application"""

        self._observer = StructureTasks()
        self._current_window = ManagerViews(self._observer)
        #self._task_ctrl = {
        #    "MAIN": self._current_window.get_form('MAIN').controller,
        #    "TASK": self._current_window.get_form('TASK').controller
        #}
        self._current_window.run()
