"""Tasks views module"""

from npyscreen import NPSAppManaged

from models import ViewMode
from .forms import TaskForm, TaskListForm


class ManagerViews(NPSAppManaged):
    """Tasks list window viewer"""

    def __init__(self, observer):
        """ Initialize form view for Tasks list (first main window)"""

        super(ManagerViews, self).__init__()
        self._observer = observer
        self._list_form = None
        self._task_form = None
        self._list = None
        self._task = None

    def onStart(self):
        """Start npyscreen Application window there"""

        self._list_form = TaskListForm(self._observer)
        self._task_form = TaskForm(self._observer)
        self._list = self.registerForm('MAIN', self._list_form)
        self._task = self.registerForm('TASK', self._task_form)

    def get_form(self, form):
        """Return asked form"""

        if form == "MAIN":
            return self._list_form
        elif form == "TASK":
            return self._task_form
