"""Tasks Window views modules"""

from npyscreen import ActionForm, SplitForm, FormWithMenus
from npyscreen.wgbutton import MiniButtonPress
from npyscreen import TitleFixedText, TitleDateCombo, MultiLineEditableBoxed, MultiLineEditableTitle

from controllers import TaskTable, ActionButton
from controllers import TaskController, TasksListController


class TaskForm(ActionForm):
    """MainWindow view"""

    def __init__(self, observer):

        super(TaskForm, self).__init__()
        self._controller = TaskController(self, observer)

    @property
    def controller(self):

        return self._controller

    def create(self):

        self.name = 'Task Report'
        self._uuid = self.add(TitleFixedText, name="UUID:", editable=False)
        self._project = self.add(TitleFixedText, name="Project:")
        self._status = self.add(TitleFixedText, name="Status:")
        self._date_entry = self.add(TitleDateCombo,
                                    name="Created date:",
                                    editable=False)
        self._date_start = self.add(TitleDateCombo,
                                    name="Started date:",
                                    editable=False)
        self._date_end = self.add(TitleDateCombo,
                                  name="Closed date:",
                                  editable=True)
        self._description = self.add(MultiLineEditableBoxed,
                                     slow_scroll=True,
                                     max_height=8,
                                     name="Description")
        self._urgency = self.add(TitleFixedText, name="Urgency:")
        self._tags = self.add(MultiLineEditableTitle, name="Tags:")

    ####  ALL THE METHOD ARE REDIRECTED TO THE CONTROLLER OBJECT   ######

    def beforeEditing(self):

        self._controller.beforeEditing()

    def afterEditing(self):

        self._controller.afterEditing()

    def on_ok(self):

        self._controller.on_ok()

    def on_cancel(self):

        self._controller.on_cancel()


class TaskListForm(SplitForm, FormWithMenus):
    """View to add task"""


    class OK_Button(MiniButtonPress):
        """Override OK Button call to add method to override"""

        def whenPressed(self):

            return self.parent.on_ok()

    SplitForm.OKBUTTON_TYPE = OK_Button

    def __init__(self, observer):

        super(TaskListForm, self).__init__()
        # Define the Controller object to redirect actions methods (MVC)
        self._controller = TasksListController(self, observer)

    @property
    def controller(self):

        return self._controller

    def create(self):
        """Create widgets content form"""

        # name of the Form window
        self.name = "TaskWarrior List"
        # define the hline up to the action buttons down to this Form
        self.draw_line_at = self.curses_pad.getmaxyx()[0] - 3
        # exec method: while_waiting in 25.5 seconds (the maximum)
        self.keypress_timeout = 255
        ###########  Build Form Widgets  ############################
        self._text = self.add(TitleFixedText,
                              name = "Menu:", 
                              value= "Task list" )
        self._grid = self.add(TaskTable, height=10)
        self._menu = self.new_menu(name="Action", shortcut="^M")
        self._menu.addItem("Show/Edit", self.show_edit, "^S")
        self._menu.addItem("Report Logs", self.report_logs, "^L")

    ####  ALL THE METHOD ARE REDIRECTED TO THE CONTROLLER OBJECT   ######

    def while_editing(self, *args, **keywords):
        """Called each time after a widget has been edited"""

        self._controller.while_editing(args, keywords)

    def beforeEditing(self):

        self._controller.beforeEditing()

    def afterEditing(self):

        self._controller.afterEditing()

    def while_waiting(self):
        """Called after keypress_timeout rich down to 0 value (1/10 second unit)"""

        self._controller.while_waiting()

    def show_edit(self):

        self._controller.menu_show_edit()

    def report_logs(self):

        self._controller.menu_report_logs()

    def on_ok(self):

        self._controller.on_ok()
