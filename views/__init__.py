"""Views modules initialization"""

# MainView is the MainWindow (the first one) where all can be call from
# AddView is view to create new task
# InfoView is view of info task
# EditView is view to edit task
from .forms import TaskForm, TaskListForm
# ManagerView is the one to contain all the other views
from .manager_views import ManagerViews

