"""Task object model"""

from enum import Enum
from json import loads
from subprocess import run as command


class ViewMode(Enum):
    """Mode opf View possibilities to show"""

    LIST = 1
    SHOW = 2
    NEW = 3
    EDIT = 4


class TaskWarrior:
    """Task command call model object"""

    def __init__(self, filter):
        """Construct command taskwarrior call with a filter"""

        self._tasks = loads(command(["task", filter, "export"],
                                    capture_output=True,
                                    check=True).stdout)

    def get(self, render_type="str"):
        """Return object formed from JSON taskwarrior called"""

        tasks = [[x['id'], 
                 x['project'], 
                 x['description'], 
                 x['urgency']] for x in self._tasks]
        if render_type == "str":
            return [str('{0:<4} {1:<10} {2:<10} {3:>4}')
                    .format(x[0], x[1], x[2], x[3]) 
                    for x in tasks]
        elif render_type == "array":
            return tasks
        elif render_type == "report":
            return self._tasks[0]


class StructureTasks():
    """Observer structure"""

    def __init__(self):

        self._data = {}
        print("Observer is on...")

    def get(self, key):

        if key in self._data:
            return self._data[key]

    def set(self, key, value):

        self._data[key] = value

    def render(self):

        print("We have:")
        print(self._data)
        #for key, value in self._data.items():
        #    print(str('{0:>16}: {1:<16}').format(key, value))

