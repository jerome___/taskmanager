# TaskManager TUI

## Description

TUI application to help using taskwarrior application.
TaskManager offer a curses interface on terminal emulator shell to manage tasks from taskwarrior.
The final idea is to be used the easy way on a tiling window manager (like i3 or awesome) and to be call from a menu (like rofi or dmenu).

## Dependencies

- [npyscreen](https://npyscreen.readthedocs.io/index.html)
- [pipenv](https://pipenv.readthedocs.io/en/latest/)
- [pipenv-to-requirements](https://pypi.org/project/pipenv-to-requirements/)
- [python-3.8](https://docs.python.org/3/) (version >= 3.4.1 should run)
- [taskwarrior](https://taskwarrior.org/)

## Design

MVC design pattern.
