"""
Task Manager would like to offer taskwarrior behaviors (not full of them at start)
Pattern of curse interface from shell terminal is:

1/ Print first list of loged in user inside a verticaly slidable box.
   Who contain also list of features to act on this list like:
   1.1/ Filter the entry of the list with criterions
   1.2/ Select one or many entries of the filtered list
   1.3/ Have at the top some button to access absolute unrelated selection actions like to:
        "Add task", "Report", "Undo last action", "History", "Summary", "Stats"
   1.3/ Access a next window to provide an action to choose on selected task entries from list

2/ Next window should appear with info about selected tasks listed,
    with a list of actions to command on each if adapted to conditions request: all of them can accept the action):
    On each line you can act for individual process toi run same window independently with info on specific selection
    2.1/ Annotate with text entry is not empty
    2.2/ Add tags with list of created tags from tag entry short text and  "+" button
    2.3/ Start/Stop toggle button to start or stop tasks
    2.4/ Modify something with list of things who can be moddificate.
    2.5/ Delete tasks button with confirmation to delete
    2.6/ Add datation with calendar widget entry
    2.7/ exportation to...
    2.8/ Done action
    2.9/  


"""

from controllers import ManagerController


def main():
    """Start ManagerController"""
    
    manager = ManagerController()


if __name__ == "__main__":
    main()
